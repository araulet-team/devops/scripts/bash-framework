# Bash Framework [WIP] [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

*Run custom scripts*

## Getting Started

```shell 
$ ./install.sh
$ dude --help
$ dude --exec do
```
