#!/bin/bash

function do_something() {
  echo "do_something called."
}

function do_something_with_args() {
  echo $1
  echo "do_with_args called."
}

function do_something_with_multiple_args() {
  args=("$@")
  echo "Number of arguments: $* - Should be split by spaces"
  echo "something_with_multiple_args called."
}
