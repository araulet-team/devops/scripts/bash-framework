#!/usr/bin/env bash

file="$(basename "${BASH_SOURCE[0]}")"
eval "function __usage_${file} ()
{
  echo '=====================================
diskspace [directory] [number to display]

Show directories ordered by disk size

- [directory]: default to ${HOME}
- [number to display]:default to 10

example
-------
dude -e diskspace /Users 20

=====================================
diskusage [directory/file]

Total disk usage in %

[directory/file]: optional

example
-------
dude -e diskusage
dude -e diskusage /Home

====================================='
};"

diskspace () {
  local src="${1:-${HOME}}"
  local top="${2:-10}"

  du -sh ${src}* 2>/dev/null | sort -rh | head -n ${top} | cat -n
}

diskusage () {
  local src="${1:-}"

  df -H $src | sort -r --key=5
}
