# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.2] - 2019-07-20
### Added
disk.bash file for a concret example
--list-details options to print methods documentation

### Fixed
Fix installation scripts
Fix arguments injected in custom methods

## [0.0.1] - 2019-06-30
### Added
Initial Commit
