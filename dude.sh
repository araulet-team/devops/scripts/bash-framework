#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

if [[ "${DEBUG_INTERNAL:-}" == "true" ]]; then
  set -o xtrace
fi;

readonly __dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly __file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
readonly __base="$(basename ${__file} .sh)"
readonly __date=$(date '+%Y-%m-%d %H:%M:%S')

# dependencies
source .env.src

#
# show how to use this installer
#

__usage() {
  cat <<EOM
  __Usage: dude [arguments]

  Arguments
  =========
  --help (-h): Display this help message
  --version (-v): Display version of the installer
  --list (-l): List methods available
  --list-details (-ld): List methods details (when documented)

  Examples
  ========
  dude --version
  dude --help
  dude --list
  dude --list-details
  dude --exec [method],[arg1],[arg2]...
EOM
  exit 0
}

#
# current framework version
#

function __version () {
  echo "0.0.2"
}

#
# list all available methods
#

function __methods_list () {
  echo -e "Available methods:\n------------------"
  declare -F | cut -d ' ' -f3 | grep -v '__'
}

function __methods_details () {
  for file in "./custom/*"
  do
    "__usage_$(basename ${file})"
  done
}

#
# load mandatory dependencies
#

source "$DUDE_DL_FOLDER/lib/logging.bash"
source "$DUDE_DL_FOLDER/lib/validation.bash"
source "$DUDE_DL_FOLDER/lib/helpers.bash"

#
# info
#

__stat () {
  info=$(cat << EOF
  =========================
  date: ${__date}
  user: ${USER:-}
  file: ${__file}
  process id: $$
  arguments count: ${#}
  arguments supplied: ${@:-no arguments supplied}
  =========================

EOF
)

  __logging_info "${info}"
}

#
# load custom scripts
#

CUSTOM_SCRIPTS="${DUDE_DL_FOLDER}/custom/*.bash"

for script in $CUSTOM_SCRIPTS
do
  source $script
done

#
# replace longer params by it's shorter version
#
for param in "$@"; do
  shift
  case "$param"  in
    "--help")               set -- "$@" "-h" ;;
    "--version")            set -- "$@" "-v" ;;
    "--exec")               set -- "$@" "-e" ;;
    "--list-details")       set -- "$@" "-d" ;;
    "--list")               set -- "$@" "-l" ;;
    *)                      set -- "$@" "$param";
  esac
done

# no params set. use help by default
[[ "${#}" -eq 0 ]] && {
  set -- "$@" "-h"
}

while getopts "e:lhvd" opt; do
  case $opt in
  h)
    __stat;
    __usage;
    exit 0
    ;;
  v)
    __version; exit 0
    ;;
  l)
    __methods_list; exit 0
  ;;
  d)
    __methods_details; exit 0
    ;;
  e)
    # all: [opts] [methods] [arg1] [arg2]
    IFS=',' all=($@)
    all=("${all[@]:1}")
    fct=${all[0]}
    args=("${all[@]:1}")

    if [[ "${DEBUG:-}" == "true" ]]; then
      set -o xtrace
    fi;

    ${fct} ${args[@]}
    ;;
  \?)
    echo "Invalid option: -${OPTARG:-$@}" >&2
    exit 1
    ;;
  :)
    echo "Option -${OPTARG:-$@} requires an argument." >&2
    exit 1
    ;;
  esac
done

