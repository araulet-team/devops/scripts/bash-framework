#!/bin/bash
set -eu

function __format_question () {
	len=${#1}
	str=$'\n'
	str+=$(printf '\n%b\n' "$1")
	str+=$'\n\n'"${mag}"'* answer ([yY]/[nN]):'"${end}"' '

	echo "$str"
}

function __generate_folder() {
	ts=$(date +"%T")
	abs_path="$(cd "$(dirname "$0")" && pwd)"
	folder="${abs_path}__tmp__${ts}"

	echo "$folder"
}

function __ask() {
	question=$(format_question "$2")
	read -p "${question}" answer

	if [[ $answer =~ (y|Y) ]]; then
		eval $1
	else
		echo -e "\n${blu}"$3"${end}\n"
	fi
}

function __abs_path() {
  echo "$(cd "$(dirname "$0")" && pwd)"
}

