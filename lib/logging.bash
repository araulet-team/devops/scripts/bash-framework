#!/usr/bin/env bash

color_red=$'\e[1;31m'
color_grn=$'\e[1;32m'
color_yel=$'\e[1;33m'
color_blu=$'\e[1;34m'
color_mag=$'\e[1;35m'
color_cyn=$'\e[1;36m'
color_end=$'\e[0m'

__logging_error () {
  local msg=$1
  echo -e "${color_red}${msg}${color_end}\n"
}

__logging_warning () {
  local msg=$1
  echo -e "${color_yel}${msg}${color_end}\n"
}

__logging_success () {
  local msg=$1
  echo -e "${color_grn}${msg}${color_end}\n"
}

__logging_info () {
  local msg=$1
  echo -e "${color_cyn}${msg}${color_end}\n"
}

__logging_usage () {
cat << EOF
file: `basename $0`

  function availables:
  - logging_info
  - logging_success
  - logging_warning
  - logging_error

  usage
  -----
  logging_info "hello world"

EOF
}
