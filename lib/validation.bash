#!/usr/bin/env bash

E_WRONG_ARGS=85

# Ensure script is executed with the good number of arguments
# If not exit with an error code
__ensure_script_args () {
  local args_wanted=$1
  local args_given=$2

  if [ ${args_given} -ne "${args_wanted}" ]
  then
    echo "Usage: \"`basename $0`\" script needs ${args_wanted} arguments. Arguments given: ${args_given}"
    # `basename $0` is the script's filename.
    exit $E_WRONG_ARGS
  fi
}

# Ensure that calling script is run as root.
# If not exit with an error code
__ensure_root_user () {
  ROOT_UID=0    # Only users with $UID 0 have root privileges.
  E_NOTROOT=87  # Non-root exit error.

  # Run as root, of course.
  if [ "$UID" -ne "$ROOT_UID" ]
  then
    echo "Must be root to run this script."
    exit $E_NOTROOT
  fi
}

__validation_usage () {
cat << EOF
file: `basename $0`

  function availables:
  - ensure_script_args
  - ensure_root_user

  usage
  -----
  ensure_script_args "argument wanted" " argument given"
  ensure_script_args 3 "${#}"
  ensure_root_user

EOF

}
