#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

if [[ "${DEBUG:-}" == "true" ]]; then
  set -o xtrace
fi;

readonly __dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly __file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
readonly __base="$(basename ${__file} .sh)"
readonly __date=$(date '+%Y-%m-%d %H:%M:%S')

# dependencies
source .env.src
source "${__dir}/lib/logging.bash"

__stat () {
  info=$(cat << EOF
  \n
  =========================
  date: ${__date}
  user: ${USER:-}
  file: ${__file}
  process id: $$
  arguments count: ${#}
  arguments supplied: ${@:-no arguments supplied}
  =========================\n
EOF
)

  __logging_info "${info}"
}

__stat

#
# uninstall steps
#

__logging_warning "...uninstall start..."
if [[ -e "$DUDE_EXECUTABLE" ]]; then
  __logging_info "====> $DUDE_EXECUTABLE executable exist in ${DUDE_BIN_LN}. remove it."
  sudo rm "${DUDE_BIN_LN}"
else
  __logging_warning "====> $DUDE_EXECUTABLE executable do not exist."
fi;


# copy all files to a specific folder


if [[ -d "$DUDE_DL_FOLDER" ]]; then
  __logging_info "====> folder "$DUDE_DL_FOLDER" exist. remove it."
  rm -rf $DUDE_DL_FOLDER
else
  __logging_warning "====> folder "$DUDE_DL_FOLDER" do not exist."
fi;

__logging_warning "...uninstall finished..."
