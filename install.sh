#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

if [[ "${DEBUG:-}" == "true" ]]; then
  set -o xtrace
fi;

readonly __dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly __file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
readonly __base="$(basename ${__file} .sh)"
readonly __date=$(date '+%Y-%m-%d %H:%M:%S')

# dependencies
source .env.src
source "${__dir}/lib/logging.bash"

__stat () {
  info=$(cat << EOF
  \n
  =========================
  date: ${__date}
  user: ${USER:-}
  file: ${__file}
  process id: $$
  arguments count: ${#}
  arguments supplied: ${@:-no arguments supplied}
  =========================\n
EOF
)

  __logging_info "${info}"
}


#
# setup
#

PWD="$(pwd)"

#
# show how to use this installer
#

__usage() {
  info=$(cat <<EOM
  =========================
  installer is going to download to copy all files
  in: "$DUDE_DL_FOLDER".
  Will set dude.sh file as executable.

  Usage: $PWD/install.sh

  Arguments
  =========
  none

  Examples
  =======
  ./install.sh
  ./uninstall.sh
  =========================
EOM
)

  __logging_info "${info}"
}

__stat
__usage

#
# copy all files to a specific folder
#

__logging_warning "...installation start..."

if [[ -d "$DUDE_DL_FOLDER" ]]; then
  __logging_error "====> a folder called "$DUDE_DL_FOLDER" already exist"
  exit 1
else
  __logging_info "====> folder "$DUDE_DL_FOLDER" do not exist. create it."
  mkdir -p $DUDE_DL_FOLDER
  cp -R . $DUDE_DL_FOLDER
fi;

#
# create executable
#

if [[ -d "$DUDE_EXECUTABLE" ]]; then
  __logging_warning "====> $DUDE_EXECUTABLE executable already exist in ${DUDE_BIN_LN}."
  exit 0
else
  __logging_info  "====> create "dude" executable"
  sudo ln -s "${DUDE_EXECUTABLE}" "${DUDE_BIN_LN}"
fi;

__logging_warning "...Installation finished successfully!..."

